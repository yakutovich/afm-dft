#!/usr/bin/python

import numpy as np
import os
import math

# ====================== constants

eVA_Nm               =  16.0217657
CoulombConst         = -14.3996448915;

scrpt_loc=os.path.dirname(__file__)
# default parameters of simulation
params={
'PBC': False,
'nPBC' :       np.array( [      1,        1,        1 ] ),
'cellA':       np.array( [ 12.798,  -7.3889,  0.00000 ] ),
'cellB':       np.array( [ 12.798,   7.3889,  0.00000 ] ),
'cellC':       np.array( [      0,        0,      5.0 ] ),
'relaxCO':True,
'relaxSUBSTRATE':False,

'scanStep': np.array( [ 0.10, 0.10, 0.10 ] ),
'scanMin': np.array( [   0.0,     0.0,    5.0 ] ),
'scanMax': np.array( [  20.0,    20.0,    8.0 ] ),
'gridA':       np.array( [] ),
'gridB':       np.array( [] ),
'gridC':       np.array( [] ),
'scheduler' : 'none',
'tip': 'Cu2CO',
'ntip': 4,
'kCantilever'  :  1800.0, 
'f0Cantilever' :  23165.0,
'Amplitude'    :  0.2,
'plotSliceFrom':  16,
'plotSliceTo'  :  22,
'plotSliceBy'  :  1,
'imageInterpolation': 'bicubic',
'colorscale'   : 'gray',
}

def loadParams( fname ):
        print " >> OVERWRITING SETTINGS by "+fname
	fin = open(fname,'r')
	FFparams = []
	for line in fin:
		words=line.split()
		if len(words)>=2:
			key = words[0]
			if key in params:
				val = params[key]
				print key,' is class ', val.__class__
				if   isinstance( val, bool ):
					word=words[1].strip()
					if (word[0]=="T") or (word[0]=="t"):
						params[key] = True
					else:
						params[key] = False
					print key, params[key], ">>",word,"<<"
				elif isinstance( val, float ):
					params[key] = float( words[1] )
					print key, params[key], words[1]
				elif   isinstance( val, int ):
					params[key] = int( words[1] )
					print key, params[key], words[1]
				elif isinstance( val, str ):
					params[key] = words[1]
					print key, params[key], words[1]
				elif isinstance(val, np.ndarray ):
					if val.dtype == np.float:
						params[key] = np.array([ float(words[1]), float(words[2]), float(words[3]) ])
						print key, params[key], words[1], words[2], words[3]
					elif val.dtype == np.int:
						print key
						params[key] = np.array([ int(words[1]), int(words[2]), int(words[3]) ])
						print key, params[key], words[1], words[2], words[3]
	fin.close()
        min_x = params['scanMin'][0]*100       # min x-coordinate of grid [pm]
        max_x = params['scanMax'][0]*100    # max x-coordinate of grid [pm]
        del_x = params['scanStep'][0]*100       # grid spacing [pm]
        num_grdpnt_x = int(round((max_x - min_x)/del_x))+1	# number of grid points
        params['gridA'] = np.linspace(min_x, max_x, num_grdpnt_x)		# array with grid points
        
        
        min_y = params['scanMin'][1]*100    
        max_y = params['scanMax'][1]*100  
        del_y = params['scanStep'][1]*100 
        num_grdpnt_y = int(round((max_y - min_y)/del_y))+1
        params['gridB'] = np.linspace(min_y, max_y, num_grdpnt_y)
        
        # Vertical Grid (Position of 1st Cu Atom in Tip above Substrate)
        min_z = params['scanMin'][2]*100    
        max_z = params['scanMax'][2]*100  
        del_z = params['scanStep'][2]*100 
        num_grdpnt_z = int(round((max_z - min_z)/del_z))+1
        params['gridC'] = np.linspace(min_z, max_z, num_grdpnt_z)

        with open(scrpt_loc+"/tips/"+params['tip']+".xyz") as f:
            params['ntip']=int(f.readline().split()[0])
