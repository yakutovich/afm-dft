#!/usr/bin/env python

"""
Run CP2K Geometry Optimizations and Energy/Force Calculations
"""

import numpy as np
import os
import shutil
import subprocess
import glob
import math
from dft_afm.xyz import XYZParser
import dft_afm.common as cmn

'''
Variable Definitions (to specify)
'''
#sample_tip="Pentacene_Cu2CO"
#tip = cmn.params['tip']

cmn.loadParams( 'params.ini' )

num_tip_atom = cmn.params['ntip']     # number of tip-atoms


pos_x=cmn.params['gridA']
pos_y=cmn.params['gridB']
pos_z=cmn.params['gridC'][::-1]

scheduler=cmn.params['scheduler']


num_proc_per_calc = 1		# number of processes for each calculation		
num_parallel_proc = 60		# number of parallel computations on lateral grid
cp2k_bin = 'cp2k'


'''
Parallelisation
'''
import multiprocessing as mp

# Lateral (x,y)-Grid
num_grdpnt_x = len(pos_x)
num_grdpnt_y = len(pos_y)
num_grdpnt_z = len(pos_z)
xy = np.ndarray(shape=(num_grdpnt_x*num_grdpnt_y, 2))
for i in range(0, num_grdpnt_x):
        for j in range(0, num_grdpnt_y):
                xy[i*len(pos_y)+j, 0] = pos_x[i]
                xy[i*len(pos_y)+j, 1] = pos_y[j]

'''
CP2K Files
'''

sample_tip_ef_inp = 'substrate_tip_ef.inp'        # cp2k input file for calculating energy and forces of sample and tip
tip_ef_inp = 'tip_ef.inp'                      # cp2k input file for calculating energy and forces of tip
sample_tip_opt_inp = 'substrate_tip_opt.inp'      # cp2k input file for geometry optimization of sample and tip

sample_tip_ef_out = 'substrate_tip_ef.out'	# cp2k output file for energy and force calculation of sample and tip
tip_ef_out = 'tip_ef.out'			# cp2k output file for energy and force calculation of tip
sample_tip_opt_out = 'substrate_tip_opt.out'	# cp2k output file for geometry optimization of sample and tip

sample_tip_ef_wfn = 'sustrate_tip-RESTART.wfn'	# wavefunction file of energy and force calculation of sample and tip
tip_ef_wfn = 'tip-RESTART.wfn'			# wavefunction file of energy and force calculation of tip
sample_tip_opt_wfn = 'substrate_tip-RESTART.wfn'	# wavefunction file of geometry optimization of sample and tip

sample_tip_xyz = 'substrate_tip.xyz'			# .xyz file with input coordinates for sample and tip
tip_xyz = 'tip-pos.xyz'				# .xyz file with input coordinates for tip
sample_tip_opt_xyz = 'substrate_tip-pos-1.xyz'		# .xyz file with output coordinates of geometry optimization of sample and tip
sample_tip_relaxed_xyz = 'substrate_tip_relaxed.xyz'	# .xyz file with relaxed input coordinates for sample and tip
tip_relaxed_xyz = 'tip_relaxed.xyz'			# .xyz file with relaxed input coordinates for tip


'''
Run CP2K
'''

# Extract Relaxed Coordinates of Tip after the Geometry Optimization
def rlxcoord():
        for xi in pos_x:
                for yi in pos_y:
                        for zi in pos_z:
                                path = './x' + str(xi) + 'pm/y' + str(yi) + 'pm/z' + str(zi) + 'pm'
                                if( not os.path.isfile(path+'/'+sample_tip_opt_xyz) ):
                                        print ("WARNING: Please manually rerun the calculaions in the folder: {}".format(path)) 
                                        continue
                                count = 0
                                f_sample_tip = open(path + '/' + sample_tip_relaxed_xyz, 'w')
                                f_tip = open(path + '/' + tip_relaxed_xyz, 'w')
                                f = open(path + '/' + sample_tip_opt_xyz, 'r')
                                s = f.read()
				# Extract last Set of Coordinates from Geometry Optimization
                                for (last_natoms, last_comment, last_atomiter) in XYZParser.parse_iter(s):
                                        pass
                                f_sample_tip.write(" {}\n  {}\n".format(last_natoms, last_comment))
                                f_tip.write(" {}\n {}\n".format(num_tip_atom, last_comment))
                                for (sym, (x, y, z)) in last_atomiter:
                                        f_sample_tip.write(" {} {: >30.20f} {:>30.20f} {:>30.20f} \n".format(sym, x, y, z))
                                        if (count >= last_natoms-num_tip_atom):
                                                f_tip.write(" {} {: >30.20f} {:>30.20f} {:>30.20f} \n".format(sym, x, y, z))
					count += 1
                                f_sample_tip.close()
                                f_tip.close()
	

# Copy Relaxed Coordinates of Tip to next z-Grid Point as Initial Guess
def rlxcoord_nextdir(pathz, pathznext, z, znext):
	delz = znext-z									
        file = open(pathznext+'/'+sample_tip_xyz, 'w') 
	f = open(pathz+'/'+sample_tip_opt_xyz, 'r')
        s = f.read()
        count = 0
	# Extract last Set of Coordinates from Geometry Optimization
        for (last_natoms, last_comment, last_atomiter) in XYZParser.parse_iter(s):
                pass
	file.write(" {}\n  {}\n".format(last_natoms, last_comment))
        for (sym, (x, y, z)) in last_atomiter:
                if (count >= last_natoms-num_tip_atom):
			z += delz/100
                file.write(" {} {: >30.20f} {:>30.20f} {:>30.20f} \n".format(sym, x, y, z))
		count += 1
	file.close()

	
# Run Energy/Force Calculations of Tip and Sample
def sample_tip_ef(xy):
	path = 'x'+str(xy[0])+'pm/y'+str(xy[1])+'pm' 
	for zi in pos_z:
		pathz = path+'/z'+str(zi)+'pm'
		if os.path.exists(pathz+'/'+sample_tip_ef_out):
			os.remove(pathz+'/'+sample_tip_ef_out)
		# Start CP2K Energy/Force Calculation of Sample and Tip
                if (scheduler == 'torque'):
                    subprocess.call('qsub submit_ef ; while [[ 1  ]] ; do grep  ENDED '+sample_tip_ef_out+' &> /dev/null && break; sleep 2 ; done;', shell=True, cwd=pathz) 
                if (scheduler == 'slurm'):
                    subprocess.call('sbatch submit_ef ; while [[ 1  ]] ; do grep  ENDED '+sample_tip_ef_out+' &> /dev/null && break; sleep 2 ; done;',shell=True,cwd=pathz)
                else:
                    subprocess.call('mpirun -np '+str(num_proc_per_calc)+' '+cp2k_bin+' -o '+sample_tip_ef_out+' '+tip_ef_inp, shell=True, cwd=pathz)
		filelist = glob.glob(pathz + '/substrate_tip-RESTART.wfn.bak*')
                for f in filelist:
                        os.remove(f)


# Run Energy/Force Calculation of Tip
def tip_ef(xy):
        path = 'x'+str(xy[0])+'pm/y'+str(xy[1])+'pm' 
	for zi in pos_z:          
                pathz = path+'/z'+str(zi)+'pm'
                if os.path.exists(pathz+'/'+tip_ef_out):
                        os.remove(pathz+'/'+tip_ef_out)
		# Start CP2K Energy/Force Calculation of Tip
                if (scheduler == 'torque'):
                    subprocess.call('qsub submit_tip ; while [[ 1  ]] ; do grep  ENDED '+tip_ef_out+' &> /dev/null && break; sleep 2 ; done;', shell=True, cwd=pathz) 
                if (scheduler == 'slurm'):
                    subprocess.call('sbatch submit_tip ; while [[ 1  ]] ; do grep  ENDED '+tip_ef_out+' &> /dev/null && break; sleep 2 ; done;',shell=True,cwd=pathz)
                else:
                    subprocess.call('mpirun -np '+str(num_proc_per_calc)+' '+cp2k_bin+' -o '+tip_ef_out+' '+tip_ef_inp, shell=True, cwd=pathz)

		# Use Optimized Wavefunction as Restart for Calculation at the next z-Grid Point
		if (zi != pos_z[num_grdpnt_z-1]):
			pathznext = path+'/z'+str(pos_z[int(np.where(zi==pos_z)[0])+1])+'pm'
			if os.path.exists(pathz+'/'+tip_ef_wfn):
	        		shutil.copyfile(pathz+'/'+tip_ef_wfn, pathznext+'/'+tip_ef_wfn)
		filelist = glob.glob(pathz + '/tip-RESTART.wfn.bak*')
                for f in filelist:
                        os.remove(f)
	

# Relax CO at Tip with a Geometry Optimization
def sample_tip_opt(xy):
        path = 'x'+str(xy[0])+'pm/y'+str(xy[1])+'pm'
        for zi in pos_z:           
                pathz = path+'/z'+str(zi)+'pm'	
                if os.path.exists(pathz+'/'+sample_tip_opt_out):
                        os.remove(pathz+'/'+sample_tip_opt_out)
		# Start CP2K Geometry Optimization of CO at Tip
                if (scheduler == 'torque'):
                    subprocess.call('qsub submit_opt ; while [[ 1  ]] ; do grep  ENDED '+sample_tip_opt_out+' &> /dev/null && break; sleep 2 ; done;', shell=True, cwd=pathz) 
                if (scheduler == 'slurm'):
                    subprocess.call('sbatch submit_opt ; while [[ 1  ]] ; do grep  ENDED '+sample_tip_opt_out+' &> /dev/null && break; sleep 2 ; done;',shell=True,cwd=pathz)
                else:
                    subprocess.call('mpirun -np '+str(num_proc_per_calc)+' '+cp2k_bin+' -o '+sample_tip_opt_out+' '+tip_ef_inp, shell=True, cwd=pathz)
                # Use Optimized Wavefunction as Restart for Calculation at the next z-Grid Point and Optimized Coordinates as Initial Guess
		if (zi != pos_z[num_grdpnt_z-1]):
			pathznext = path+'/z'+str(pos_z[int(np.where(zi==pos_z)[0])+1])+'pm'
			if os.path.exists(pathz+'/'+sample_tip_opt_wfn):
                        	shutil.copyfile(pathz+'/'+sample_tip_opt_wfn, pathznext+'/'+sample_tip_opt_wfn)        
			if os.path.exists(pathz+'/'+sample_tip_opt_xyz):
				rlxcoord_nextdir(pathz, pathznext, zi, pos_z[int(np.where(zi==pos_z)[0])+1]) 	# create a new .xyz coordinate file in the next folder by shifting obtained optimized coordinates of tip
		filelist = glob.glob(pathz + '/sample_tip-RESTART.wfn.bak*')
                for f in filelist:
                        os.remove(f)


# Parallelisation over Lateral Grid

def run_sample_tip_ef():
	pool = mp.Pool(num_parallel_proc)
        pool.map(sample_tip_ef, xy)		# run (x,y)-gridpoints in parallel
        pool.close()
        pool.join()

def run_tip_ef():
       	pool = mp.Pool(num_parallel_proc)
      	pool.map(tip_ef, xy)
       	pool.close()
       	pool.join()

def run_sample_tip_opt():
     	pool = mp.Pool(num_parallel_proc)
      	pool.map(sample_tip_opt, xy)    
    	pool.close()
       	pool.join()
	
if __name__=="__main__":
	run_sample_tip_opt()
	rlxcoord()	# extract optimized coordinates for energy/force calculations
	run_sample_tip_ef()
	run_tip_ef()
