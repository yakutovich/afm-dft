#!/usr/bin/env python

import numpy as np
import re
import math
import dft_afm.common as cmn

'''
Variable Definitions (to specify)
'''
#sample_tip = "Pentacene_Cu2CO"


cmn.loadParams( 'params.ini' ) 

#tip = cmn.params['tip']
num_tip_atoms = cmn.params['ntip']     # number of tip-atoms
atom_types = np.array([('O', 1), ('C', 2), ('Cu1', 4), ('Cu2', 3)], dtype=[('f1', 'S10'),('f2', 'i4')])


pos_x=cmn.params['gridA']
pos_y=cmn.params['gridB']
pos_z=cmn.params['gridC']



'''
CP2K Output Files
'''
sample_tip_ef_out = 'substrate_tip_ef.out'	# cp2k output file with energy and forces of sample and tip
tip_ef_out = 'tip_ef.out'			# cp2k output file with energy and forces of tip


'''
General Functions
'''
def path_xyz(x, y, z):
        path = './x' + str(x) + 'pm/y' + str(y) + 'pm/z' + str(z) + 'pm'
        return path

def path(pos, val):
        path = '/' + pos + str(val) + 'pm'
        return path


'''
Forces from Output Files
'''

# Extract Forces from the CP2K Output Files
def extract_forces(path_output):
        with open(path_output, 'r') as f:
                output = f.read()
        force_matches = re.findall(r"ATOMIC FORCES in.*\n\n.*\n((?:\s{4,6}.*\n)*) SUM OF ATOMIC FORCES", output)
        if len(force_matches) != 0:
                if len(force_matches) > 1:
                        print("More than one 'ATOMIC FORCES' entries were found in the output file. Probably due to the fact that outputs are appended to the same file. The last entry is returned. PATH: " + path_output)
        else:
                print("'ATOMIC FORCES' entry was not found in the CP2K output file. PATH: " + path_output)
                return
        # Parse the Force String
        force_string = force_matches[-1]
        lines = force_string.splitlines()
        forces = np.zeros([len(lines), 3])
        for i, line in enumerate(lines):
                words = line.split()
                z = float(words[-1])
                y = float(words[-2])
                x = float(words[-3])
                forces[i, 0] = x
                forces[i, 1] = y
                forces[i, 2] = z
        return forces


# Force Contributions on the Different Tip Atoms
def frc_diff_atoms():
	for a in range(0, len(atom_types)):
		file = open('frc_'+(atom_types[a])[0]+'.txt', 'w')
        	file.write('# Coordinates of Tip & Forces \n# x [pm] | y [pm] | z [pm] | Fx on '+(atom_types[a])[0]+' [pN] | Fy on '+(atom_types[a])[0]+' [pN] | Fz on '+(atom_types[a])[0]+' [pN]\n')
		for xi in pos_x:
               		for yi in pos_y:
                       		for zi in pos_z:
                                	path = path_xyz(xi, yi, zi)
                                	path_output_sample_tip = path + '/' + sample_tip_ef_out
					path_output_tip = path + '/' + tip_ef_out
					forces_sample_tip = extract_forces(path_output_sample_tip)
					num_atoms =  len(forces_sample_tip)
					forces_tip = extract_forces(path_output_tip)
                                	file.write(str(xi) + '\t'+str(yi) + '\t'+str(zi))
					for m in range(0, 3):
						forces = (forces_sample_tip[num_atoms-(atom_types[a])[1], m]-forces_tip[num_tip_atoms-(atom_types[a])[1], m])*82387	# interaction forces [pN]
                                		file.write("{:>15.8f}".format(forces))
                                	file.write('\n')

# Forces on the Tip
def frc_tip():
	file = open('frc_tip.txt', 'w')
	file.write('# Coordinates of Tip & Forces \n # x [pm] | y [pm] | z [pm] | Fx on tip [pN] | Fy on tip [pN] | Fz on tip [pN] \n')
	for xi in pos_x:
		for yi in pos_y:
			for zi in pos_z:
				path = path_xyz(xi, yi, zi)
				path_output_sample_tip = path + '/' + sample_tip_ef_out
				path_output_tip = path + '/' + tip_ef_out
				forces_sample_tip = extract_forces(path_output_sample_tip)
				num_atoms =  len(forces_sample_tip)
				forces_tip = extract_forces(path_output_tip)
				file.write(str(xi) + '\t' + str(yi) + '\t'+str(zi))
				for m in range(0, 3):
					forces = 0.0
					for a in range(0, num_tip_atoms):
						forces += (forces_sample_tip[num_atoms-1-a, m]-forces_tip[num_tip_atoms-1-a, m])*82387
					file.write("{:>15.8f}".format(forces))	
				file.write('\n')		


if __name__=="__main__":
	frc_diff_atoms()
	frc_tip()
