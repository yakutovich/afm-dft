#!/usr/bin/env python

"""
Frequency Shift 2-dim Map
"""

import numpy as np
from scipy import interpolate
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import math
import dft_afm.common as cmn


# Plot Settings
font = {'family' : 'serif',
        'size'   : 16}

plt.rc('text', usetex = True)
plt.rc('font', **font)
plt.rc('xtick', labelsize = 20)
plt.rc('ytick', labelsize = 20)
plt.rc('axes', labelsize = 'large')


'''
Variables (to specify)
'''

# Project Names

cmn.loadParams( 'params.ini' )

# Experimental Constants
k0 = cmn.params['kCantilever']          # spring constant [N/m]
f0 = cmn.params['f0Cantilever']              # resonance frequency [Hz]


# Smoothing for Interpolation of Force Data Points
smoothing = 1.0
# Lateral Grid
pos_x = cmn.params['gridA']
num_grdpnt_x = len(pos_x)
pos_y = cmn.params['gridB']
num_grdpnt_y = len(pos_y)
posz_eval_array = cmn.params['gridC']
#print posz_eval_array
# Textfile with Frequency Shift 2-Dimensional Map

def txtfile_freqshift(freqshift_data):
	for zi in posz_eval_array:
		file = open('freqshift_z' + str(zi) + 'pm.txt', 'w')
		file.write('# Coordinated of Tip & Frequency Shift at z = ' + str(zi) + 'pm \nx [pm] | y [pm] | z [pm] | Frequency Shift [Hz] \n')
		for xi in pos_x:		
			for yi in pos_y:
				file.write(str(xi)+'\t'+str(yi)+'\t'+str(zi))
				dfreq_xyz = float(freqshift_data[np.where(pos_x == xi), np.where(pos_y == yi), np.where(posz_eval_array == zi)])
				file.write("{:>15.8f}".format(dfreq_xyz))
				file.write('\n')
		file.close()


# Plot of Frequency Shift 2-Dimensional Map together with Atomic Positions of Pentacene

def plt_freqshift_fromtxt_pentacene():
        N = 1000
        for zi in posz_eval_array:
		pentacene_data = np.genfromtxt('pentacene.xyz', skip_header = 2, unpack = True, dtype = "S2,f8,f8,f8")
                x, y, z, dfreq_xyz = np.loadtxt('freqshift_z' + str(zi) + 'pm.txt', skiprows = 2, unpack=True)
                x = x/100.0
		y = y/100.0
		xi = np.linspace(x.min(), x.max(), N)
                yi = np.linspace(y.min(), y.max(), N)
                dfreq_xyz_i = interpolate.griddata((x, y), dfreq_xyz,
                (xi[None,:], yi[:,None]), method='cubic')
                fig = plt.figure()
#       for i in range(0, len(pentacene_data)):
#       	if ((pentacene_data[i])[0] == 'H'):
#       		plt.scatter(float((pentacene_data[i])[1])-10.0, float((pentacene_data[i])[2])-10.0, color = 'blue')
#       	else:
#       		plt.scatter(float((pentacene_data[i])[1])-10.0, float((pentacene_data[i])[2])-10.0, color = 'red')
#
		im = plt.imshow(dfreq_xyz_i, extent=(x.min(),x.max(),y.min(),y.max()), origin='lower', cmap='gray')
		plt.xlabel("x [\AA]")
                plt.ylabel("y [\AA]")
		plt.axis([x.min(), x.max(), y.min(), y.max()])
		
		# Colorbar
		from mpl_toolkits.axes_grid1 import make_axes_locatable
		divider = make_axes_locatable(plt.gca())
		cax = divider.append_axes("right", "5%", pad="2%")
		cbar = plt.colorbar(im, cax=cax)
		plt.tight_layout()
		cbar.set_label('$\Delta$f [Hz]', labelpad=-20)#, rotation=360)
		cbar.set_ticks([math.ceil(dfreq_xyz.min()), math.floor(dfreq_xyz.max())])
                plt.savefig('./figures/freqshft_z'+str(zi)+'pm_pentacene.png')
                plt.close(fig)



# Vertical Forces from DFT Calculation

def load_fz(fname):
    	# force data
    	x, y, z, fx, fy, fz = np.loadtxt(fname, skiprows = 2, unpack = True) 
	return x, y, z, fz


# Calculation of Frequency Shift Curve dFreq(z) at given (x,y)-Grid Point from the Force Gradient Method

def freqshift_grdpnt(fz, z, posz_eval, smoothing_spln):
#        print "GG"
#        print posz_eval
#        print z
#        print fz
        spl = interpolate.UnivariateSpline(z, fz, k = 2, s = smoothing_spln)
#        print spl
        der = spl.derivatives(posz_eval)
        dfreq = -f0/(2*k0)*der[1]
        return dfreq


# Calculation of a 2-Dimensional Frequency Shift Map

def calc_freqshft(x,y,z,fz):
#        print x
#        print y
	freqshift_data = np.ndarray(shape=(num_grdpnt_x, num_grdpnt_y, len(posz_eval_array)))
#        print freqshift_data
    
	for xi in pos_x:
		for yi in pos_y:
#                        print "before where {} {}".format(xi,yi)
			idx = np.where(np.isclose(x,xi) & np.isclose(y,yi))[0]	# gridpoint (x,y)
#                        print "idx {}".format(idx)
#                        print type(idx)
#                        print "inx1 {}".format(idx[1])
			fz_xy = fz[idx]		# vertical force at given gridpoint (x,y)
#                        print "fz {}".format(fz_xy)
			z_xy = z[idx]		# z positions at given gridpoint (x,y)
			for zi in posz_eval_array:
                		dfreq_xyz = freqshift_grdpnt(fz_xy, z_xy, posz_eval = zi, smoothing_spln = smoothing)	# frequency shift at given gridpoint (x,y,z) 
                                freqshift_data[np.where(pos_x == xi), np.where(pos_y == yi), np.where(posz_eval_array == zi)] = dfreq_xyz		# 3-dim array with frequency shift at all (x,y,z) 
        txtfile_freqshift(freqshift_data)       # write frequency shift data to text file


if __name__=="__main__":
        from optparse import OptionParser
        parser = OptionParser()
        parser.add_option("-i", "--input", action="store", type="string", help="input "
                          "file",default=None)
        (options, args) = parser.parse_args()
        if options.input:
            data=np.genfromtxt(options.input,usecols=[0,1,2,3,4,5],skip_header=2)
        else:
            raise ValueError("please, specify the input filename")
    
        x, y, z, fz = load_fz(options.input)
	calc_freqshft(x,y,z,fz)
	plt_freqshift_fromtxt_pentacene()

