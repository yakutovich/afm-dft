#!/usr/bin/env python

"""
Grid for Running Geometry Optimizations and Energy/Force Calculations
"""

import numpy as np
import os
import sys
import shutil
import math
import dft_afm.common as cmn
from optparse import OptionParser


scrpt_loc=os.path.dirname(__file__)

HELP_MSG="""Use this program in the following way:
"""+os.path.basename(__file__) +""" -i <filename> 

Supported file fromats are:
   * xyz """
parser = OptionParser()
parser.add_option( "-i", "--input", action="store", type="string", help="format of input file")

(options, args) = parser.parse_args()
opt_dict = vars(options)
    

if options.input==None:
    sys.exit("ERROR!!! Please, specify the input file with the '-i' option \n\n"+HELP_MSG)
'''
Variable Definitions (to specify)
'''
is_xyz  = options.input.lower().endswith(".xyz")
if not is_xyz :
    sys.exit("ERROR!!! Unknown format of the input file\n\n"+HELP_MSG)

cmn.loadParams( 'params.ini' )
pos_x=cmn.params['gridA']
pos_y=cmn.params['gridB']
pos_z=cmn.params['gridC']
num_tip_atoms = cmn.params['ntip']		# number of atoms in the tip

scheduler=cmn.params['scheduler']

# Reference Positions of Sample Molecule, Corresponds to (x,y,z) = (0,0,0) on the Grid
refpos_x = 0.0		
refpos_y = 0.0		
refpos_z = 0.0


'''
CP2K Input Files
'''


#sample_tip="Pentacene_Cu2CO"
tip=cmn.params['tip']

sample=options.input[:-4]                   # calculation name is taken from the name of the file with the input coordinates
sample_tip_ef_inp = 'substrate_tip_ef.inp'	# cp2k input file for calculating energy and forces of sample and tip
tip_ef_inp = 'tip_ef.inp'			# cp2k input file for calculating energy and forces of tip
sample_tip_opt_inp = 'substrate_tip_opt.inp'	# cp2k input file for geometry optimization of sample and tip
sample_tip_xyz='substrate_tip.xyz'
sample_xyz = sample + '.xyz'	        # cp2k input coordinates of sample and tip
tip_xyz = tip + '-pos.xyz'


'''
General Functions
'''
def path_xyz(x, y, z):
	path = './x' + str(x) + 'pm/y' + str(y) + 'pm/z' + str(z) + 'pm'
	return path

def path(pos, val):
	path = '/' + pos + str(val) + 'pm' 
	return path


'''
.xyz Files with Tip-Coordinates at Different xyz-Positions
'''

def calc_coord_bak():
        coord = np.genfromtxt('./'+ sample_tip_xyz , skip_header = 2, dtype = 'S2, f8, f8, f8')
        num_atoms = len(coord)		# number of atoms in substrate
	for xi in pos_x:
		for yi in pos_y:
			for zi in pos_z:
				new_coord = np.ndarray(shape=(num_tip_atoms, 3))
				for i in range(0, num_tip_atoms):
					new_coord[i, 0] = refpos_x + xi/100.0 - ((coord[num_atoms - num_tip_atoms])[1] - (coord[num_atoms - num_tip_atoms + i])[1])
					new_coord[i, 1] = refpos_y + yi/100.0 - ((coord[num_atoms - num_tip_atoms])[2] - (coord[num_atoms - num_tip_atoms + i])[2])
					new_coord[i, 2] = refpos_z + zi/100.0 - ((coord[num_atoms - num_tip_atoms])[3] - (coord[num_atoms - num_tip_atoms + i])[3])
				path = path_xyz(xi, yi, zi)
				file_sample_tip = open(path + '/substrate-tip.xyz', 'w')
				file_tip = open(path + '/tip.xyz', 'w')
                                file_sample_tip.write(str(num_atoms) + '\n\n')
                                file_tip.write(str(num_tip_atoms) + '\n\n')
				for m in range(0, num_atoms):
                                	if (m >= num_atoms - num_tip_atoms):
						file_sample_tip.write("{} {: >15.8f} {:>15.8f} {:>15.8f}\n".format((coord[m])[0], new_coord[m + num_tip_atoms - num_atoms, 0], new_coord[m + num_tip_atoms - num_atoms, 1], new_coord[m + num_tip_atoms - num_atoms, 2]))
						file_tip.write("{} {: >15.8f} {:>15.8f} {:>15.8f}\n".format((coord[m])[0], new_coord[m + num_tip_atoms - num_atoms, 0], new_coord[m + num_tip_atoms - num_atoms, 1], new_coord[m + num_tip_atoms - num_atoms, 2]))
					else:
						file_sample_tip.write("{} {: >15.8f} {:>15.8f} {:>10.8f}\n".format((coord[m])[0], (coord[m])[1], (coord[m])[2], (coord[m])[3]))
				file_sample_tip.close()
				file_tip.close()

def calc_coord():
        coord = np.genfromtxt('./'+ sample_xyz , skip_header = 2, dtype = 'S2, f8, f8, f8')
        num_sample_atoms=len(coord)
        coord_tip = np.genfromtxt(scrpt_loc+'/dft_afm/tips/'+tip+'.xyz' , skip_header = 2, dtype = 'S2, f8, f8, f8')
	for xi in pos_x:
		for yi in pos_y:
			for zi in pos_z:
				new_coord = np.ndarray(shape=(num_tip_atoms, 3))
				for i in range(0, num_tip_atoms):
					new_coord[i, 0] = xi/100.0 + coord_tip[i][1]
					new_coord[i, 1] = yi/100.0 + coord_tip[i][2]
					new_coord[i, 2] = zi/100.0 + coord_tip[i][3]
				path = path_xyz(xi, yi, zi)
				file_sample_tip = open(path + '/'+ sample_tip_xyz, 'w')  
				file_tip = open(path + '/tip.xyz', 'w')
                                file_sample_tip.write(str(num_sample_atoms+num_tip_atoms) + '\n\n')
                                file_tip.write(str(num_tip_atoms) + '\n\n')
				for m in range(0, num_sample_atoms):
                                	file_sample_tip.write("{} {: >15.8f} {:>15.8f} {:>15.8f}\n".format((coord[m])[0], coord[m][1], coord[m][2], coord[m][3]))
				for m in range(0, num_tip_atoms):
                                	file_sample_tip.write("{} {: >15.8f} {:>15.8f} {:>15.8f}\n".format((coord_tip[m])[0], new_coord[m, 0], new_coord[m, 1], new_coord[m, 2]))
                                	file_tip.write("{} {: >15.8f} {:>15.8f} {:>15.8f}\n".format((coord_tip[m])[0], new_coord[m, 0], new_coord[m, 1], new_coord[m, 2]))
				file_sample_tip.close()
				file_tip.close()

'''
Folders for Running Calculations
'''

def mk_grid():
        coord = np.genfromtxt('./'+ sample_xyz , skip_header = 2, dtype = 'S2, f8, f8, f8')
        num_sample_atoms=len(coord)
        coord_tip = np.genfromtxt(scrpt_loc+'/dft_afm/tips/'+tip+'.xyz' , skip_header = 2, dtype = 'S2, f8, f8, f8')
        num_tip_atoms=len(coord_tip)
        with open(scrpt_loc+'/dft_afm/cp2k/' + sample_tip_ef_inp) as finp:
                ST_EF_input=finp.readlines()
        with open(scrpt_loc+'/dft_afm/cp2k/' + tip_ef_inp) as finp:
                T_EF_input=finp.readlines()
        with open(scrpt_loc+'/dft_afm/cp2k/' + sample_tip_opt_inp) as finp:
                ST_OPT_input=finp.readlines()
        i=0
        while i < len( ST_EF_input):
            ST_EF_input[i]=ST_EF_input[i].replace("CELLATOREPLACE",
            "{}".format(' '.join(str(x) for x in cmn.params['cellA'])))
            ST_EF_input[i]=ST_EF_input[i].replace("CELLBTOREPLACE",
            "{}".format(' '.join(str(x) for x in cmn.params['cellB'])))
            ST_EF_input[i]=ST_EF_input[i].replace("CELLCTOREPLACE",
            "{}".format(' '.join(str(x) for x in cmn.params['cellC'])))
#            print ST_EF_input[i],
            i+=1
        
        i=0
        while i < len( T_EF_input):
            T_EF_input[i]=T_EF_input[i].replace("CELLATOREPLACE",
            "{}".format(' '.join(str(x) for x in cmn.params['cellA'])))
            T_EF_input[i]=T_EF_input[i].replace("CELLBTOREPLACE",
            "{}".format(' '.join(str(x) for x in cmn.params['cellB'])))
            T_EF_input[i]=T_EF_input[i].replace("CELLCTOREPLACE",
            "{}".format(' '.join(str(x) for x in cmn.params['cellC'])))
#            print T_EF_input[i],
            i+=1
        
        
        i=0
        while i < len( ST_OPT_input):
            ST_OPT_input[i]=ST_OPT_input[i].replace("CELLATOREPLACE",
            "{}".format(' '.join(str(x) for x in cmn.params['cellA'])))
            ST_OPT_input[i]=ST_OPT_input[i].replace("CELLBTOREPLACE",
            "{}".format(' '.join(str(x) for x in cmn.params['cellB'])))
            ST_OPT_input[i]=ST_OPT_input[i].replace("CELLCTOREPLACE",
            "{}".format(' '.join(str(x) for x in cmn.params['cellC'])))
            ST_OPT_input[i]=ST_OPT_input[i].replace("NFIXEDATOMS",
            "{}".format(num_sample_atoms+num_tip_atoms-2))
#            print ST_OPT_input[i],
            i+=1


	for xi in pos_x:
		path_x = '.' + path('x', xi)
		if os.path.isdir(path_x):
			shutil.rmtree(path_x)
		os.makedirs(path_x)
		for yi in pos_y:
			path_y = path_x + path('y', yi)
			if os.path.isdir(path_y):
                        	shutil.rmtree(path_y)
                	os.makedirs(path_y)
			for zi in pos_z:
				path_z = path_y + path('z', zi)
                        	if os.path.isdir(path_z):
                                	shutil.rmtree(path_z)
                        	os.makedirs(path_z)
#				print('./' + sample_tip_ef_inp+ path_z + '/' + sample_tip_ef_inp)
				with open (path_z + '/' + sample_tip_ef_inp, 'w') as ifile:
                                    ifile.writelines(ST_EF_input)
				with open (path_z + '/' + tip_ef_inp, 'w') as ifile:
                                    ifile.writelines(T_EF_input)
				with open (path_z + '/' + sample_tip_opt_inp, 'w') as ifile:
                                    ifile.writelines(ST_OPT_input)
                                if (scheduler != 'none'):
				    shutil.copyfile(scrpt_loc+'/dft_afm/schedulers/'+scheduler+'/submit_opt', path_z + '/submit_opt') 
				    shutil.copyfile(scrpt_loc+'/dft_afm/schedulers/'+scheduler+'/submit_ef',  path_z + '/submit_ef') 
				    shutil.copyfile(scrpt_loc+'/dft_afm/schedulers/'+scheduler+'/submit_tip', path_z + '/submit_tip') 


if __name__=="__main__":
	shutil.copyfile(scrpt_loc+'/dft_afm/cp2k/dftd3.dat', './dftd3.dat') 
	shutil.copyfile(scrpt_loc+'/dft_afm/cp2k/GTH_POTENTIALS', './GTH_POTENTIALS') 
	shutil.copyfile(scrpt_loc+'/dft_afm/cp2k/BASIS_MOLOPT', './BASIS_MOLOPT') 
	mk_grid()
	calc_coord()
